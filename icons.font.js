module.exports = {
  'files': [
    './webApp/fonts/**/*.svg'
  ],
  'fontName': 'myIcons',
  'classPrefix': 'myIcons-',
  'baseSelector': '.myIcons',
  'types': ['eot', 'woff', 'woff2', 'ttf', 'svg'],
  'fileName': '../../webApp/generated/fonts/myIcons/app.[fontname].[ext]',
};