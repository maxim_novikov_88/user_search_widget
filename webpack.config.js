const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CompressionPlugin = require('compression-webpack-plugin');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
require('dotenv').config({ path: '.env.development' });

module.exports = (env) => {
  const isProduction = env === 'production',
      webpack = require('webpack'),
      CleanWebpackPlugin = require("clean-webpack-plugin"),
      TSLintPlugin = require('tslint-webpack-plugin');

  const babelOptions = {
    presets: ["env", "react"],
    plugins: [
      "transform-class-properties",
      "transform-object-rest-spread",
      "babel-plugin-styled-components",
      "syntax-dynamic-import"
    ]
  };

  return {

    entry: {
        app: ['babel-polyfill', './src/js/app.tsx']
    },
    output: {
      path: path.join(__dirname, 'public', 'dist'),
      filename: "[name].bundle.js",
      publicPath: "/public/dist/"
    },
    mode: env ? env : "development",
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    // target: 'electron-renderer',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use:{
            loader: "babel-loader",
            options: babelOptions
          }
        },
        {
          test: /\.ts(x?)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: "babel-loader",
              options: babelOptions
            },
            {
              loader: "ts-loader"
            }
          ]
        },
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            // !isProduction ? 'style-loader' : MiniCssExtractPlugin.loader,
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.(eot|woff2|woff|ttf|svg|png)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                  publicPath: "./../../",
                  name: '[path][name].[ext]',
                  emitFile: false
                  // useRelativePath: true
              }
            }
          ]
        },
        {
          test: /\.font\.js/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'webfonts-loader'
          ]
        }
      ]
    },
    plugins: [
      new MiniCssExtractPlugin({
          filename: '[name].css',
      }),
      new CleanWebpackPlugin(["public/dist/chunks/*.*", "public/dist/*.*"],
      {
        verbose: true,
        dry: !isProduction,
        allowExternal: true
      }),
      new webpack.DefinePlugin({
        'process.env.FIREBASE_API_KEY': JSON.stringify(process.env.FIREBASE_API_KEY),
        'process.env.FIREBASE_AUTH_DOMAIN': JSON.stringify(process.env.FIREBASE_AUTH_DOMAIN),
        'process.env.FIREBASE_DATABASE_URL': JSON.stringify(process.env.FIREBASE_DATABASE_URL),
        'process.env.FIREBASE_PROJECT_ID': JSON.stringify(process.env.FIREBASE_PROJECT_ID),
        'process.env.FIREBASE_STORAGE_BUCKET': JSON.stringify(process.env.FIREBASE_STORAGE_BUCKET),
        'process.env.FIREBASE_MESSAGING_SENDER_ID': JSON.stringify(process.env.FIREBASE_MESSAGING_SENDER_ID)
      }),
      new TSLintPlugin({
        files: ['./src/js/**/**/*.{ts,tsx}']
      }),
      new CompressionPlugin({
        test: /\.js(\?.*)?$/i,
        exclude: /\/node_modules/
      })
    ],
    devtool: isProduction ? 'source-map' : 'inline-source-map',
    devServer: {
      contentBase: path.join(__dirname, '/'),
      historyApiFallback: true,
      publicPath: '/public/dist/',
      watchContentBase: true,
    },
    optimization: {}
  };
};