## Install and start

First of all, after you cloned project, start these command in the root, and install packages
```
 git clone git@bitbucket.org:maxim_novikov_88/user_search_widget.git
 
  - or
  
 git clone https://maxim_novikov_88@bitbucket.org/maxim_novikov_88/user_search_widget.git
 
  - and
  
 npm install
```
After that you need to start dev-server
```
 npm run dev-server
```
Now you can open your browser and look `http://localhost:8080/` page

If you want to start JavaScript Testing
```
 npm run test
```

## ToDo list
- ~~Rewrite all Javascript with Typescript~~ (almost done, need to dive deeper for studying types for dispatch and thunk functions)
- ~~Write Jest tests~~ (almost done, covered all actions, need to fix the problem with dotenv.config, firestore not seeing .env.test)
- ~~Rewrite Class to React Hooks~~ (I have rewrote with Classes, because Hooks not working well with promises in update lifecycle)
- ~~Fix bug with Sort filter~~ (Done)



