import React, { Component } from 'react';
import { connect } from "react-redux";
import SearchUsersForm from "../common/layout/SearchUsersForm";
import UsersList from "../common/layout/UsersList";
import ListPaginator from "../common/layout/ListPaginator";

import { showToastMsg } from "../../actions/toast";
import { startSortByOrFilters } from "../../actions/users";
import {IAppDefState, ISortFuncArguments} from "../../types/app";
import {TUsersDefState} from "../../types/users";


export interface IWidgetWrapper {
  app: IAppDefState;
  users: TUsersDefState;
  showToastMsg: (text: string, msgType: string) => void;
  startSortByOrFilters: (...args: ISortFuncArguments) => void;
}

class WidgetWrapper extends Component<IWidgetWrapper>{

  public componentDidMount() {
    if (!this.props.users.length) {
      this.props.startSortByOrFilters(null);
    }
  }

  public render(){
    return (
      <div className={"content-container"}>
        <SearchUsersForm />
        <UsersList/>

        <ListPaginator
          pagination={this.props.app.pagination}
          sortedBy={this.props.app.sortedBy}
          sortDirection={this.props.app.sortDirection}
          filters={this.props.app.filters}
          startSortByOrFilters={this.props.startSortByOrFilters}/>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users,
  app: state.app,
});

const mapDispatchToProps = (dispatch) => ({
  showToastMsg: (text, msgType) => dispatch(showToastMsg(text, msgType)),
  startSortByOrFilters: (sortedBy, sortDirection, filters, from) => dispatch(startSortByOrFilters(sortedBy, sortDirection, filters, from)),
});

export default connect(mapStateToProps, mapDispatchToProps)(WidgetWrapper);
