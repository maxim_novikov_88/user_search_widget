import validator from "validator";

export interface IFValidatorItem{
    field: string;
    method: string;
    validWhen: boolean;
    message: string;
    options?: [];
    multiply?: boolean;
}

export default class FormValidator {

    public validations: IFValidatorItem[];

    constructor(validations: IFValidatorItem[]){
        this.validations = validations;
    }

    public validate(state, key, prevValidation) {
        let validation;
        const multiplyRules = {};
        if (key && prevValidation){
            validation = prevValidation;

            this.validations.forEach((rule) => {

                if (rule.field === key){
                    const fieldValue = state[rule.field].toString(),
                      options = rule.options || [],
                      validationMethod = typeof rule.method === 'string' ? validator[rule.method] : rule.method,
                      isInvalid = validationMethod(fieldValue, ...options, state) !== rule.validWhen,
                      keyNotMultiply = multiplyRules[key] === undefined;

                    let multiplyBoolean = false,
                      multiplyMsg = "";

                    if (rule.multiply){
                        if (keyNotMultiply) multiplyRules[key] = [];

                        multiplyRules[key] = [
                            ...multiplyRules[key],
                            {
                                name: rule.method,
                                msg: rule.message,
                                isInvalid,
                            },
                        ];
                    }

                    if (isInvalid && keyNotMultiply) {
                        validation[rule.field] = {
                            isInvalid: true,
                            message: rule.message,
                        };
                        validation.isValid = false;
                    }

                    if (!isInvalid && keyNotMultiply || !fieldValue.length){
                        validation[rule.field] = {
                            isInvalid: false,
                            message: "",
                        };
                        validation.isValid = true;
                    }

                    if (!keyNotMultiply && fieldValue.length) {
                        multiplyRules[key].forEach((item) => {
                            if (item.isInvalid){
                                multiplyBoolean = true;
                                multiplyMsg += item.msg;
                            }
                        });

                        validation[rule.field] = {
                            isInvalid: multiplyBoolean,
                            message: multiplyMsg,
                        };
                        validation.isValid = !multiplyBoolean;
                    }
                }
            });

        }else{
            validation = this.valid();
            this.validations.forEach((rule) => {
                if (!validation[rule.field].isInvalid) {
                    const fieldValue = state[rule.field].toString();
                    const options = rule.options || [];
                    const validationMethod = typeof rule.method === 'string' ? validator[rule.method] : rule.method;

                    if (validationMethod(fieldValue, ...options, state) !== rule.validWhen) {
                        validation[rule.field] = {
                            isInvalid: true,
                            message: rule.message,
                        };
                        validation.isValid = false;
                    }
                }
            });
        }

        return validation;
    }

    public valid() {
        const validation = {};

        this.validations.map((rule) => (validation[rule.field] = { isInvalid: false, message: '' }));
        return { isValid: true, ...validation };
    }
}
