import React from "react";
import {Typeahead} from "react-bootstrap-typeahead";

export interface IFormAutocompleteInputProps {
    col?: number;
    colSm?: number;
    optsArr: string[];
    placeholder: string;
    name: string;
    currentValue: string;
    onChangeHandler: (field: string, value: string) => void;
}

export default ( {col = 6, colSm = 6, name, placeholder, currentValue, optsArr, onChangeHandler} : IFormAutocompleteInputProps ) => (
    <div className={`col-${col} col-sm-${colSm} form-group autocomplete-wrap`}>
        <Typeahead
            placeholder={placeholder}
            value={currentValue}
            onChange={(selected) => onChangeHandler(name, selected[0])}
            onInputChange={(value) => onChangeHandler(name, value)}
            options={optsArr}/>
    </div>
);
