import React from "react";

export interface IFormInputProps {
    col?: number;
    colSm?: number;
    placeholder: string;
    name: string;
    value: string;
    onChangeHandler: (field: string, value: string) => void;
    className?: string;
    errorMessage?: string;
}

export default ({ col = 6, colSm = 6, placeholder, name, value, onChangeHandler, className, errorMessage} : IFormInputProps) => (
  <div className={`col-${col} col-sm-${colSm} form-group ${className}`}>
    <input className={"form-control"}
           placeholder={placeholder}
           type="text" name={name} value={value}
           onChange={(e) => onChangeHandler(name, e.target.value)}/>
    <span className={"error-block"}>{errorMessage}</span>
  </div>
);
