import React from "react";
import { SingleDatePicker } from 'react-dates';

interface IFocused {
    focused: null | boolean;
}

export interface IFormAutocompleteInputProps {
    col?: number;
    colSm?: number;
    placeholder: string;
    dateField: Date | null;
    focused: boolean;
    onDateChange: (Date) => void;
    onFocusChange: (IFocused) => void;
    id: string;
}

export default ({col = 4, colSm = 6, id, placeholder, dateField, focused, onDateChange, onFocusChange} : IFormAutocompleteInputProps) => (
    <div className={`col-${col} col-sm-${colSm} form-group`}>
        <SingleDatePicker
            placeholder={placeholder}
            date={dateField || null}
            onDateChange={(date) => onDateChange(date)}
            focused={focused}
            onFocusChange={({ focused: focusedField }) => onFocusChange({ focused: focusedField })}
            numberOfMonths={1}
            isOutsideRange={() => false}
            displayFormat={"DD/MM/YYYY"}
            id={id}/>
    </div>
);
