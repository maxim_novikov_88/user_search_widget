import React, { Component } from 'react';
import { connect } from "react-redux";
import { StickyContainer, Sticky } from "react-sticky";
import moment from "moment";
import Spinner from "../Spinner";

import ListHeadCell from "./ListHeadCell";

import { startSortByOrFilters } from "../../../actions/users";
import {IAppDefState, ISortFuncArguments} from "../../../types/app";
import {IUser, TUsersDefState} from "../../../types/users";


export interface IUserListProps {
  app: IAppDefState;
  users: TUsersDefState;
  startSortByOrFilters: (...args: ISortFuncArguments) => void;
}


class UsersList extends Component<IUserListProps> {

  public render(){
    const sortColumns = [
      { field: "account.firstName", name: "Name"},
      { field: "account.surname", name: "Surname"},
      { field: "account.email", name: "Email"},
      { field: "account.dob", name: "Phone"},
      { field: "account.residenceCountry", name: "Country"},
      { field: "account.residenceCity", name: "City"},
      { field: "lastActive", name: "Last Active"},
    ];

    return (
      <div className={"users m20-0 " + this.props.app.loader}>

        <StickyContainer>

          <Sticky>
            {({ style, isSticky } : { style: {}, isSticky: boolean}) => {

              return (
                <div className={"sticky-table-head" + (isSticky ? " sticky" : "")} style={style}>
                  <div className={"list-title"}>
                    {
                      sortColumns.map((column, i) => {
                        return (
                            <ListHeadCell
                              key={i}
                              app={this.props.app}
                              field={column.field}
                              name={column.name}
                              startSortByOrFilters={this.props.startSortByOrFilters}/>
                          );
                      })
                    }
                  </div>
                </div>
              );
            }}
          </Sticky>
            {
              !!this.props.users.length &&
              <table className={"users-list"}>
                <tbody>
                {
                  this.props.users.map((user: IUser, i) => {
                    const account = user.account;
                    return (
                      <tr key={i} className={"list-item"}>
                        <td className={"name "} title={account.firstName}>{account.firstName}</td>
                        <td className={"surname"} title={account.surname}>{account.surname}</td>
                        <td className={"email"} title={account.email}>{account.email}</td>
                        <td className={"phone"} title={account.dob.toString()}>{account.dob}</td>
                        <td className={"country"} title={account.residenceCountry}>{account.residenceCountry}</td>
                        <td className={"city"} title={account.residenceCity}>{account.residenceCity}</td>
                        <td className={"last-active"} title={moment(user.lastActive).format("DD.MM.YYYY")}>{moment(user.lastActive).format("DD.MM.YYYY")}</td>
                      </tr>
                    );
                  })
                }
                </tbody>
              </table>
            }
            {
              !this.props.users.length &&
              <div className={"users-list pt-20"}>
                Users list is Empty!
              </div>
            }
            <Spinner/>
        </StickyContainer>
      </div>
    );
  }
}

const mapStateToProps = (state: {app: {}; users: {}}) => ({
  app: state.app,
  users: state.users,
});

const mapDispatchToProps = (dispatch: any) => ({
  startSortByOrFilters: ( sortedBy: string | null,
                          sortDirection: string,
                          filters: []): void => dispatch(startSortByOrFilters(sortedBy, sortDirection, filters, null)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
