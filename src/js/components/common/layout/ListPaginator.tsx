import React from "react";
import {IFiltersObj, IPaginationObj, ISortFuncArguments} from "../../../types/app";

export interface IListPaginatorProps {
    pagination: IPaginationObj[];
    sortedBy: null | string;
    sortDirection: string;
    filters: IFiltersObj[];
    startSortByOrFilters: (...args: ISortFuncArguments) => void;
}

const ListPaginator = ({ pagination, sortedBy, sortDirection, filters, startSortByOrFilters} : IListPaginatorProps) => {
    return (
        <ul className={"paginator mb-20"}>
            {
                (!!pagination.length && pagination.length !== 1) &&
                    pagination.map((item: IPaginationObj, i) => {
                        return (
                            <li key={i}
                                className={item.active ? "active" : ""}
                                onClick={() => !item.active && startSortByOrFilters(sortedBy, sortDirection, filters, item)}>
                                {item.index}
                            </li>
                        );
                    })
            }
        </ul>
    );
};

export default ListPaginator;
