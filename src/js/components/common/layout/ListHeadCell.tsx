import React, { Component } from 'react';
import {IAppDefState, ISortFuncArguments} from "../../../types/app";

export interface IListCellsProps {
  app: IAppDefState;
  startSortByOrFilters: (...args: ISortFuncArguments) => void;
  field: string;
  name: string;
}

export interface IListCellsState {
  direction: string;
}

export default class ListHeadCell extends Component<IListCellsProps, IListCellsState>{

  public state = {
    direction: "",
  };

  public onClickHandler = () => {
    if (this.state.direction === "" || this.props.app.sortedBy !== this.props.field){
      this.props.startSortByOrFilters(this.props.field, "asc", this.props.app.filters);
      this.setState({direction: "asc"});
    }else if (this.state.direction === "asc"){
      this.props.startSortByOrFilters(this.props.field, "desc", this.props.app.filters);
      this.setState({direction: "desc"});
    }else{
      this.props.startSortByOrFilters(null, "", this.props.app.filters);
      this.setState({direction: ""});
    }
  }

  public render(){
    return (
      <div
        className={
          `${this.state.direction} sorted ${(this.props.app.sortedBy === this.props.field ? "active " : "")} ${this.props.field.replace("account.", "")}`
        }
        onClick={this.onClickHandler}>
        <span className={"dib"}>{this.props.name}</span>
        <span className={"arrows-wrap"}>
          <span className={"myIcons myIcons-caret-arrow-up"}/>
          <span className={"myIcons myIcons-caret-arrow-down"}/>
        </span>
      </div>
    );
  }
}
