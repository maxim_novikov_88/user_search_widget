import React, { Component } from 'react';
import { connect } from "react-redux";
import FormValidator from "../libs/FormValidator";
import moment from "moment";

import {showToastMsg} from "../../../actions/toast";
import {startSortByOrFilters} from "../../../actions/users";
import {IAppDefState, ISortFuncArguments} from "../../../types/app";
import FormInput from "../form/FormInput";
import FormAutocompleteInput from "../form/FormAutocompleteInput";
import FormSingleDatePicker from "../form/FormSingleDatePicker";

export interface ISearchUsersFormProps {
  app: IAppDefState;
  startSortByOrFilters: (...args: ISortFuncArguments) => void;
  showToastMsg: (text: string, msgType: string) => void;
}

export interface IPrevState {
  send: {};
}

class SearchUsersForm extends Component<ISearchUsersFormProps>{

  public validator = new FormValidator([
    {
      field: "account.email",
      method: 'isEmail',
      validWhen: true,
      message: "Enter a correct email address.",
    },
    {
      field: 'account.dob',
      method: 'isNumeric',
      validWhen: true,
      message: "Enter a correct phone number.",
    },
  ]);

  public state = {
    send : {
      "account.firstName": "",
      "account.surname": "",
      "account.email": "",
      "account.dob": "",
      "account.residenceCountry": "",
      "account.residenceCity": "",
      dateFrom: null,
      dateTo: null,
    },
    focusedFrom: null,
    focusedTo: null,
    validation: this.validator.valid(),
  };

  public onSubmitHandler = (e) => {
    e.preventDefault();
    const filters = [];
    for (const key in this.state.send){
      if (this.state.send.hasOwnProperty(key) && this.state.send[key] && key !== "dateFrom" && key !== "dateTo"){
        filters.push({
          field: key,
          operand: "==",
          value: ( key === "account.dob" ? Number(this.state.send[key]) : this.state.send[key]),
        });
      }
    }

    if (this.state.send.dateFrom) filters.push({field: "lastActive", operand: ">=", value: moment(this.state.send.dateFrom).valueOf()});
    if (this.state.send.dateTo) filters.push({field: "lastActive", operand: "<=", value: moment(this.state.send.dateTo).valueOf()});

    if (this.state.validation.isValid){
      this.props.startSortByOrFilters(null, "asc", filters);
    }else {
      this.props.showToastMsg("You should fix errors before submit.", "error");
    }
  }

  public onChangeHandler = (field, value) => {
    const validation = this.validator.validate({[field]: value}, field, this.state.validation);
    this.setState((prevState: IPrevState) => ({
      send: {
        ...prevState.send,
        [field]: value,
      },
      validation,
    }));
  };

  public onClearFiltersHandler = (e) => {
    e.preventDefault();
    const validation = this.validator.valid();

    const send = {
      "account.firstName": "",
      "account.surname": "",
      "account.email": "",
      "account.dob": "",
      "account.residenceCountry": "",
      "account.residenceCity": "",
      dateFrom: null,
      dateTo: null,
    };
    this.props.startSortByOrFilters(null);
    this.setState({send, validation});
  };

  public render() {
    const countriesArr = this.props.app.autocomplete.countries || [];

    const searchUsersSimpleFields = {
      "account.firstName": "Name",
      "account.surname": "Surname",
      "account.email": "Email",
      "account.dob": "Phone",
      "account.residenceCity": "City",
    };

    return (
      <form className="default-form m20-0" onSubmit={this.onSubmitHandler}>
        <div className="row ">

          {
            Object.keys(searchUsersSimpleFields)
              .map( (field, i) => {
                const fieldsObj = {
                  name: field,
                  placeholder: searchUsersSimpleFields[field],
                  value: this.state.send[field],
                  colSm: (field === "account.email" ? 12 : 6),
                  onChangeHandler: this.onChangeHandler,
                  className: (this.state.validation[field] && this.state.validation[field].isInvalid ? "has-error" : ""),
                  errorMessage: (this.state.validation[field] ? this.state.validation[field].message : ""),
                };
                return <FormInput key={i} {...fieldsObj}/>;
              })
          }

          <FormAutocompleteInput
              placeholder={"Country"}
              name={"account.residenceCountry"}
              currentValue={this.state.send["account.residenceCountry"]}
              onChangeHandler={this.onChangeHandler}
              optsArr={countriesArr}/>

          {
            ["From", "To"].map((field, i) => (
              <FormSingleDatePicker
                  key={i}
                  dateField={this.state.send[`date${field}`]}
                  focused={this.state[`focused${field}`]}
                  placeholder={`Date ${field.toLowerCase()}`}
                  onDateChange={(date) => this.setState((prevState: IPrevState) => ({ send: {...prevState.send, [`date${field}`]: date}}))}
                  onFocusChange={({ focused: focusedField }) => this.setState({ [`focused${field}`] : focusedField })}
                  id={`date${field}`}/>
            ))
          }

          <div className={"form-group col-2 col-sm-3"}>
            <button type={"submit"} className={"form-control myIcons myIcons-enter-arrow submit-btn"}/>
          </div>
          <div className={"form-group col-2 col-sm-3"}>
            <button onClick={this.onClearFiltersHandler} className={"form-control myIcons myIcons-cancel cancel-btn"}/>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = (state) => ({
  users: state.users,
  app: state.app,
});

const mapDispatchToProps = (dispatch) => ({
  showToastMsg: (text, msgType) => dispatch(showToastMsg(text, msgType)),
  startSortByOrFilters: (sortedBy, sortDirection, filters, from) => dispatch(startSortByOrFilters(sortedBy, sortDirection, filters, from)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchUsersForm);
