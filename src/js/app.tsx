require('../../icons.font');
import 'react-dates/lib/css/_datepicker.css';
import './../sass/styles.scss';
import 'react-dates/initialize';
import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';
import { Provider } from "react-redux";

const store = configureStore();
const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);

ReactDOM.render(jsx, document.getElementById('app'));
