import fireBase from 'firebase/app';
import "firebase/firestore";
// import 'firebase/auth';

//temporary for jest(not working dotenv)
const conf = {
    apiKey: "AIzaSyA-14R_gelwDq61iT8H1tBIF6vkMctQe8Q",
    authDomain: "users-search-widget.firebaseapp.com",
    databaseURL: "https://users-search-widget.firebaseio.com",
    projectId: "users-search-widget",
    storageBucket: "users-search-widget.appspot.com",
    messagingSenderId: "543828631974",
};

// Initialize Firebase
const config = conf || {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
};

fireBase.initializeApp(config);
const dataStore = fireBase.firestore();

export {fireBase, dataStore};
