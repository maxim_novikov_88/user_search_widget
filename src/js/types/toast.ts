
export interface IToastDefState {
    text: string;
    msgType: string;
}

export type TToastDefAction = Partial<IToastDefState> & {
    type: string;
};
