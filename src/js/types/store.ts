import {IToastDefState} from "./toast";
import {IAppDefState} from "./app";


export interface IStore {
    users?: [];
    toast?: IToastDefState;
    app?: IAppDefState;
}
