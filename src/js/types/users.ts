

export interface IUser {
    id?: string;
    lastActive: number;
    account: {
        firstName: string;
        surname: string;
        email: string;
        dob: number;
        residenceCountry: string;
        residenceCity: string;
        address1?: string;
        address2?: string;
        defaultRefundMethodId?: string;
        displayName?: string;
        passportNo?: string;
    };
    email?: string;
    name?: string;
    surname?: string;
    firstName?: string;
    meta?: {};
}

export type TUsersDefState = IUser[];
