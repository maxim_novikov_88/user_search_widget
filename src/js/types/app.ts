import WhereFilterOp = firebase.firestore.WhereFilterOp;
import {QueryDocumentSnapshot} from "@firebase/firestore-types";

export type ISortFuncArguments = [(string | null)?, string?, IFiltersObj[]?, (null | IPaginationObj)?];

export interface IAutocomplete {
    [key: string]: string[];
}

export interface IFiltersObj {
    field: string;
    operand: WhereFilterOp;
    value: number | string;
}

export interface IPaginationObj {
    index: number;
    active: boolean;
    doc: null | QueryDocumentSnapshot;
}

export interface IAppDefState {
    mediaSize: string;
    pagination: IPaginationObj[];
    sortDirection: string;
    sortedBy: string | null;
    filters: IFiltersObj[];
    loader: string;
    autocomplete: IAutocomplete;
}

export type TAppDefAction = Partial<IAppDefState> & {
    type: string;
    docs?: QueryDocumentSnapshot[];
    itemsPerPage?: number;
    currentIndex?: number
};
