import React, { Component } from 'react';
import { connect } from "react-redux";
import { BrowserRouter, Route } from 'react-router-dom';

import { ToastContainer, toast } from 'react-toastify';
import Media from 'react-media';

import { resetToastMsg } from "../actions/toast";
import { setMediaSize } from "../actions/app";

import WidgetWrapper from "../components/pages/WidgetWrapper";
import {IToastDefState} from "../types/toast";

export interface IAppRouter{
  toast: IToastDefState;
  resetToastMsg: () => void;
  setMediaSize: (mediaSize: string) => void;
}

class AppRouter extends Component<IAppRouter>{
  public componentDidUpdate() {
    if (this.props.toast.text){
      toast[this.props.toast.msgType](this.props.toast.text, {
        onClose: () => {
          this.props.resetToastMsg();
        },
      });
    }
  }

  public render() {
    return (
      <BrowserRouter>
        <div id={"users-widget-wrapper"}>

          <Media
            query="(max-width: 767px)"
            onChange={(matches) => {
              if (matches) {
                this.props.setMediaSize("mobile");
              } else {
                this.props.setMediaSize("desktop");
              }
            }}/>

          <ToastContainer className="toast-top-right toast-container" position={"top-right"} autoClose={1600}/>

          <main className={"main"}>
            <Route component={WidgetWrapper}/>
          </main>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => ({
  toast: state.toast,
});

const mapDispatchToProps = (dispatch) => ({
  resetToastMsg: () => dispatch(resetToastMsg()),
  setMediaSize: (mediaSize) => dispatch(setMediaSize(mediaSize)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AppRouter);

