import {IUser} from "../types/users";

export default (state: IUser[] = [], action: { type: string; users?: IUser[]}) => {
  let users;
  switch (action.type) {

    case "SET_ALL_USERS":
      users = action.users;
      return users;

    default:
      return state;
  }
};
