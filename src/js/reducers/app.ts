import {IAppDefState, TAppDefAction, IPaginationObj, IFiltersObj, IAutocomplete} from "../types/app";

export default (state: IAppDefState = {
  mediaSize: "desktop",
  sortedBy: null,
  pagination: [],
  sortDirection: "",
  filters: [],
  loader: "",
  autocomplete: {} as IAutocomplete,
}, action: TAppDefAction) :  IAppDefState => {

  let mediaSize: string ,
      pagination: IPaginationObj[],
      sortedBy: string,
      filters: IFiltersObj[],
      loader: string,
      sortDirection: string,
      autocomplete: IAutocomplete;

  switch (action.type) {
    case "SET_MEDIA_SIZE":
      mediaSize = action.mediaSize;
      return {
        ...state,
        mediaSize,
      };

    case "SET_PAGINATION":
      pagination = [ ...Array(Math.ceil(action.docs.length / action.itemsPerPage)).keys() ].map(( btn ) => ({ // get math sequences and build pagination
        index: btn + 1,
        doc: btn ? action.docs[btn * action.itemsPerPage] : null,
        active: (btn + 1) === action.currentIndex,
      }));
      return {
        ...state,
        pagination,
      };

    case "SET_LOADER":
      loader = action.loader;
      return {
        ...state,
        loader,
      };

    case "SET_AUTOCOMPLETE":
      autocomplete = action.autocomplete;
      return {
        ...state,
        autocomplete,
      };

    case "SET_APP_PROPS":
      sortDirection = action.sortDirection;
      sortedBy = action.sortedBy;
      filters = action.filters;
      loader = "";
      return {
        ...state,
        sortedBy,
        sortDirection,
        filters,
        loader,
      };

    default:
      return state;
  }
};
