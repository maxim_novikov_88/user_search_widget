import {
  setMediaSize,
  setPagination,
  setLoader,
  setAppProps,
} from "../../actions/app";
import {QueryDocumentSnapshot} from "@firebase/firestore-types";

test("should get user's media queries", () => {
  const mediaSize = setMediaSize("mobile");

  expect(mediaSize).toEqual({
    type: "SET_MEDIA_SIZE",
    mediaSize: "mobile",
  });

});

test("should set pagination filter", () => {
  const docs = [] as QueryDocumentSnapshot[],
      currentIndex = 1,
      itemsPerPage = 30,
    paginationFilter = setPagination([] as QueryDocumentSnapshot[], 1, 30);

  expect(paginationFilter).toEqual({
    type: "SET_PAGINATION",
    docs,
    currentIndex,
    itemsPerPage,
  });
});

test("should set sort app props", () => {

  const sortDirection = "",
      filters = [],
      sortedBy = null,
      setProps = setAppProps();

  expect(setProps).toEqual({
    type: "SET_APP_PROPS",
    sortedBy, filters, sortDirection,
  });
});

test("should set loader", () => {
  const loader = setLoader("pending");
  expect(loader).toEqual({
    type: "SET_LOADER",
    loader: "pending",
  });
});
