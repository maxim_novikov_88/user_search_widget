import {
  setAllUsers,
  startSortByOrFilters,
} from "../../actions/users";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import {dataStore} from "../../firebase/firebase";
import {users} from "../__mocks__/users";
import {IUser} from "../../types/users";
import {WhereFilterOp} from "@firebase/firestore-types";

const dataUsers: IUser[] = users;

const createMockStore = configureMockStore([thunk]),
    defaultUsersState = dataUsers;

beforeAll((done) => {
    const promise0 = new Promise((resolve) => {
        dataStore.collection("testUsers").doc("0").set(dataUsers[0]).then(() => { resolve(); });
    });
    const promise1 = new Promise((resolve) => {
        dataStore.collection("testUsers").doc("1").set(dataUsers[1]).then(() => { resolve(); });
    });
    Promise.all([promise0, promise1]).then(() => { done(); });
});

test("should set all available users", () => {
    const allUsers = setAllUsers(dataUsers);
    expect(allUsers).toEqual({
        type: "SET_ALL_USERS",
        users: dataUsers,
    });
});

test("should get user with name filter", (done) => {
  const store = createMockStore(defaultUsersState),
    filters = [{
      field: "account.surname",
      operand: "==" as WhereFilterOp,
      value: "FLORENCE",
    }];

  store.dispatch(startSortByOrFilters(null, "", filters, null))
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({ type: 'SET_LOADER', loader: 'pending' });

        return dataStore.collection("testUsers").where("account.surname", "==", "FLORENCE" ).get();
      }).then((snapshot) => {
        const docs = snapshot.docs.map((doc) => ({...doc.data()}));
        expect(docs).toEqual([dataUsers[0]]);
        done();
      })
      .catch(() => {
        // print(e.message);
      });
});
