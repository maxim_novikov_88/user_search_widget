import {resetToastMsg, showToastMsg} from "../../actions/toast";

test("should set toast msg", () => {
  const text = "msg text",
    msgType = "success",
    showToastFunc = showToastMsg(text, msgType);

  expect(showToastFunc).toEqual({
    type: "SHOW_TOAST_MSG",
    text,
    msgType,
  });
});

test("should reset toast msg", () => {
  const showToastFunc = resetToastMsg();

  expect(showToastFunc).toEqual({
    type: "RESET_TOAST_MSG",
  });
});
