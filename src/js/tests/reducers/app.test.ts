import appReducer from "../../reducers/app";
import {app} from "../__mocks__/app";
import {QueryDocumentSnapshot} from "@firebase/firestore-types";

test("should set mediaSize, state property ", () => {
    const mediaSize = "mobile",
        action = {
            type: "SET_MEDIA_SIZE",
            mediaSize,
        };

    const state = appReducer(app, action);

    expect(state).toEqual({
        ...app,
        mediaSize,
    });
});

test("should set pagination, state property", () => {
    const pagination = [],
        docs = [] as QueryDocumentSnapshot[],
        currentIndex = 1,
        itemsPerPage = 30,
        action = {
            type: "SET_PAGINATION",
            docs,
            currentIndex,
            itemsPerPage,
        };

    const state = appReducer(app, action);

    expect(state).toEqual({
        ...app,
        pagination,
    });
});

test("should set loader, state property", () => {
    const loader = "pending",
        action = {
            type: "SET_LOADER",
            loader,
        };

    const state = appReducer(app, action);

    expect(state).toEqual({
        ...app,
        loader,
    });
});

test("should set autocomplete, state property", () => {
    const autocomplete = { countries: ["Russia", "United Kingdom"]},
        action = {
            type: "SET_AUTOCOMPLETE",
            autocomplete,
        };

    const state = appReducer(app, action);

    expect(state).toEqual({
        ...app,
        autocomplete,
    });
});

test("should set sortDirection, sortedBy, filters, state property", () => {
    const sortDirection = "asc",
        sortedBy = "account.firstName",
        filters = [],
        action = {
            type: "SET_APP_PROPS",
            sortDirection,
            sortedBy,
            filters,
        };

    const state = appReducer(app, action);

    expect(state).toEqual({
        ...app,
        sortDirection,
        sortedBy,
        filters,
    });
});
