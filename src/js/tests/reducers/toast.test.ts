import toastReducer, {defStateToast} from "../../reducers/toast";

test("should set and show toast message", () => {
    const action = {
            type: "RESET_TOAST_MSG",
        };

    const state = toastReducer(defStateToast, action);

    expect(state).toEqual(defStateToast);
});

test("should reset toast message", () => {
    const text = "this message will shown",
        msgType = "success",
        action = {
            type: "SHOW_TOAST_MSG",
            text,
            msgType,
        };

    const state = toastReducer(defStateToast, action);

    expect(state).toEqual({
        ...defStateToast,
        text,
        msgType,
    });
});
