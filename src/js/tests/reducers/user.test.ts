import {users} from "../__mocks__/users";
import usersReducer from "../../reducers/users";
import {IUser} from "../../types/users";

test("should set users, state property ", () => {
    const usersDefState: IUser[] = [],
        action = {
            type: "SET_ALL_USERS",
            users,
        };

    const state = usersReducer(usersDefState, action);

    expect(state).toEqual(users);
});
