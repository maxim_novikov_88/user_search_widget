import {IAppDefState, IAutocomplete} from "../../types/app";


export const app: IAppDefState = {
    mediaSize: "desktop",
    sortedBy: null,
    pagination: [],
    sortDirection: "",
    filters: [],
    loader: "",
    autocomplete: {} as IAutocomplete,
},
pagination = [
    {
        index: 1,
        doc: null,
        active: false,
    },
];