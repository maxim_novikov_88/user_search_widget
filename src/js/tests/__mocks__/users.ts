import {IUser} from "../../types/users";

export const users: IUser[] = [
    {
        "id": "0HrLojMVhMUUhyHtjCNA6uS2Bnr2",
        "account": {
            "address1": "Address 1",
            "address2": "Address Line 2",
            "defaultRefundMethodId": "",
            "displayName": "MASON FLORENCE",
            "dob": 564022808411,
            "email": "MASON.FLORENCE162@testemail.com",
            "firstName": "MASON",
            "passportNo": "",
            "residenceCity": "Sunderland",
            "residenceCountry": "United Kingdon",
            "surname": "FLORENCE",
        },
        "email": "mason.florence162@testemail.com",
        "firstName": "MASON",
        "lastActive": 1440780178946,
        "meta": {
            "creationTime": 1549400400000,
            "lastSignInTime": null,
        },
        "name": "MASON FLORENCE",
        "surname": "FLORENCE",
    },
    {
        "id": "35kpzxYEniNb583k3G62opioRHh1",
        "account": {
            "address1": "Address 1",
            "address2": "Address Line 2",
            "defaultRefundMethodId": "",
            "displayName": "AIDAN ETHAN",
            "dob": 105190082231,
            "email": "AIDAN.ETHAN972@testemail.com",
            "firstName": "AIDAN",
            "passportNo": "",
            "residenceCity": "Bristol",
            "residenceCountry": "United Kingdon",
            "surname": "ETHAN",
        },
        "email": "aidan.ethan972@testemail.com",
        "firstName": "AIDAN",
        "lastActive": 1549438565096,
        "meta": {
            "creationTime": 1549400400000,
            "lastSignInTime": null,
        },
        "name": "AIDAN ETHAN",
        "surname": "ETHAN",
    },
    {
        "id": "563JHRE9t9YVbz0y7l53JzFn1ag1",
        "account": {
            "address1": "Address 1",
            "address2": "Address Line 2",
            "defaultRefundMethodId": "",
            "displayName": "LEO ALEXANDRA",
            "dob": -196168993074,
            "email": "LEO.ALEXANDRA242@testemail.com",
            "firstName": "LEO",
            "passportNo": "",
            "residenceCity": "Bedford",
            "residenceCountry": "United Kingdon",
            "surname": "ALEXANDRA",
        },
        "email": "leo.alexandra242@testemail.com",
        "firstName": "LEO",
        "lastActive": 1484190270416,
        "meta": {
            "creationTime": 1549400400000,
            "lastSignInTime": null,
        },
        "name": "LEO ALEXANDRA",
        "surname": "ALEXANDRA",
    },
];