import {TToastDefAction} from "../types/toast";

export const showToastMsg = (text: string, msgType: string) : TToastDefAction => ({
        type: "SHOW_TOAST_MSG",
        text,
        msgType,
    }),
    resetToastMsg = () : TToastDefAction => ({
        type: "RESET_TOAST_MSG",
    });
