import {IFiltersObj, TAppDefAction} from "../types/app";
import {QueryDocumentSnapshot} from "@firebase/firestore-types";

export const setMediaSize = (mediaSize: string) : TAppDefAction => ({
  type: "SET_MEDIA_SIZE",
  mediaSize,
}),
setPagination = (docs: QueryDocumentSnapshot[], currentIndex: number, itemsPerPage: number) => ({
  type: "SET_PAGINATION",
  docs,
  currentIndex,
  itemsPerPage,
}),
setLoader = (loader: string) => ({
  type: "SET_LOADER",
  loader,
}),
setAutocomplete = (autocomplete: {}) => ({
  type: "SET_AUTOCOMPLETE",
  autocomplete,
}),
setAppProps = (sortedBy: null | string = null, sortDirection: string = "", filters: IFiltersObj[] = []) => ({
  type: "SET_APP_PROPS",
  sortedBy, sortDirection, filters,
});
