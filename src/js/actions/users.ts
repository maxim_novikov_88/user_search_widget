import {dataStore} from "../firebase/firebase";
import {setLoader, setPagination, setAutocomplete, setAppProps} from "./app";
import {IFiltersObj, IPaginationObj} from "../types/app";
import OrderByDirection = firebase.firestore.OrderByDirection;
import {Dispatch} from "redux";
import {Query} from "@firebase/firestore-types";
import {IUser} from "../types/users";

const ITEMS_PER_PAGE = 30;
const usersRef = dataStore.collection("users");

const buildQuery = (sortedBy: string | null, sortDirection: OrderByDirection, filters: IFiltersObj[] = []) => {
  let queryStore: Query = usersRef;
  const filtersArr: string[] = [];

  filters.forEach((filter: IFiltersObj) => {
    filtersArr.push(filter.field);
    queryStore = queryStore.where(filter.field, filter.operand, filter.value);
  });

  if (filtersArr.indexOf("lastActive") !== -1){ queryStore = queryStore.orderBy("lastActive", sortDirection || "asc"); }
  if (sortedBy && filtersArr.indexOf(sortedBy) === -1){ queryStore = queryStore.orderBy(sortedBy, sortDirection || "asc"); }
  return queryStore;
};

export const setAllUsers = (users: IUser[]) => ({
  type: "SET_ALL_USERS",
  users,
});

export const  startSortByOrFilters = (sortedBy: string | null, sortDirection: string, filters: IFiltersObj[], from: null | IPaginationObj = null) => {
  return (dispatch: Dispatch) => {
    dispatch(setLoader("pending"));
    let queryStore = buildQuery(sortedBy, sortDirection as OrderByDirection, filters);

    const queryListener = queryStore.onSnapshot((change) => { //listen queries for getting docs.length, and build pagination
      const countries = change.docs
          .map((doc) => doc.data().account.residenceCountry)
          .reduce((x, y) => x.includes(y) ? x : [...x, y], []);
      const currentIndex = from ? from.index : 1;

      dispatch(setAutocomplete({countries}));
      dispatch(setPagination(change.docs, currentIndex, ITEMS_PER_PAGE));
    });

    if (from && from.doc){ queryStore = queryStore.startAt(from.doc); }

    return queryStore.limit(ITEMS_PER_PAGE).get().then((documentSnapshots) => {
      const docs = documentSnapshots.docs;
      queryListener(); //unsubscribe

      if (!docs.length){
        dispatch(setAllUsers([]));
        dispatch(setAppProps());
        return false;
      }

      const users = docs.map((doc) => ({ id: doc.id, ...doc.data()}));
      dispatch(setAllUsers(users as []));
      dispatch(setAppProps(sortedBy, sortDirection, filters));
    });
  };
};
