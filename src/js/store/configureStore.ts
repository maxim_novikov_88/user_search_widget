import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import usersReducer from "../reducers/users";
import toastReducer from "../reducers/toast";
import appReducer from "../reducers/app";

import {IStore} from "../types/store";

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
  const store = createStore(
    combineReducers<IStore>({
      users: usersReducer,
      toast: toastReducer,
      app: appReducer,
    }),
    composeEnhancers(applyMiddleware(thunk)),
  );

  return store;
};
